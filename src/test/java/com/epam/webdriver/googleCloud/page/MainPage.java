package com.epam.webdriver.googleCloud.page;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private static final String URL = "https://cloud.google.com/";
    public WebDriver driver;
    @FindBy(xpath = "//input[@name='q']")
    private WebElement searchBox;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public MainPage openPage() {
        driver.get(URL);
        return this;
    }

    public SearchResultsPage searchPricingCalculator() {
        searchBox.click();
        searchBox.sendKeys("Google Cloud Platform Pricing Calculator");
        searchBox.sendKeys(Keys.ENTER);
        return new SearchResultsPage(driver);
    }

}
