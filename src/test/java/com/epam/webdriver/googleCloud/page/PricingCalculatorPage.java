package com.epam.webdriver.googleCloud.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.epam.webdriver.util.PageHelper.getTextNode;
import static com.epam.webdriver.util.PageHelper.waitUntilClick;

public class PricingCalculatorPage {
    public WebDriver driver;
    @FindBy(xpath = "//md-tab-item/div[@title='Compute Engine']")
    private WebElement computeEngineIcon;
    @FindBy(id = "input_85")
    private WebElement numberOfInstances;
    @FindBy(xpath = "//md-select[@ng-model='listingCtrl.computeServer.os']")
    private WebElement operatingSystemSoftware;
    @FindBy(xpath = "//md-option[@value='free']")
    private WebElement freeDebian;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'VM Class']")
    private WebElement provisioningModel;
    @FindBy(xpath = "//md-option[@value='regular']")
    private WebElement regular;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'Series']")
    private WebElement series;
    @FindBy(xpath = "//div[@class[contains(.,'md-active')]]//md-option[@value='n1']")
    private WebElement n1;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'Instance type']")
    private WebElement machineType;
    @FindBy(xpath = "//div[@class[contains(.,'md-active')]]//md-option[@value='CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8']")
    private WebElement n1Standard8;
    @FindBy(xpath = "//md-checkbox[@ng-model='listingCtrl.computeServer.addGPUs']")
    private WebElement addGPUsCheckbox;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'GPU type']")
    private WebElement gpyType;
    @FindBy(xpath = "//md-option[@value='NVIDIA_TESLA_P100']")
    private WebElement teslaP100;
    @FindBy(xpath = "//md-select[@placeholder = 'Number of GPUs']")
    private WebElement numberOfGPUs;
    @FindBy(xpath = "//div[@class[contains(.,'md-active')]]//md-option[@value='1']")
    private WebElement oneGPU;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'Local SSD']")
    private WebElement localSSD;
    @FindBy(id = "select_option_439")
    private WebElement twoFor375;
    @FindBy(xpath = "//md-select[@placeholder = 'Datacenter location']")
    private WebElement datacenterLocation;
    @FindBy(xpath = "//div[@class[contains(.,'md-active')]]//md-option[@value='europe-west3']")
    private WebElement frankfurt;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//md-select[@placeholder = 'Committed usage']")
    private WebElement committedUsage;
    @FindBy(xpath = "//div[@class[contains(.,'md-active')]]//md-option[@value='1']")
    private WebElement oneYear;
    @FindBy(xpath = "//form[@name='ComputeEngineForm']//button[contains(text(),'Add to Estimate')]")
    private WebElement addToEstimateButton;
    @FindBy(xpath = "//button[contains(text(),'Email')]")
    private WebElement emailButton;
    @FindBy(xpath = "//input[@name = 'description' and @type = 'email']")
    private WebElement emailInput;
    @FindBy(xpath = "//button[contains(text(),'Send Email')]")
    private WebElement sendEmailButton;
    @FindBy(xpath = "//*[contains(text(), 'Total Estimated Cost')]")
    private WebElement totalEstimatedCost;
    @FindBy(xpath = "//iframe[@src[contains(.,'/products/calculator/')]]")
    private WebElement iframe;
    @FindBy(xpath = "//iframe[@id='myFrame']")
    private WebElement myFrame;

    public PricingCalculatorPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public PricingCalculatorPage pickComputeEngineSection() {
        driver.switchTo().frame(iframe).switchTo().frame(myFrame);
        computeEngineIcon.click();
        return this;
    }

    public PricingCalculatorPage fillOutForm() {
        waitUntil(numberOfInstances).click();
        waitUntil(numberOfInstances).sendKeys("4");
        waitUntil(operatingSystemSoftware).click();
        waitUntil(freeDebian).click();
        waitUntil(provisioningModel).click();
        waitUntil(regular).click();
        waitUntil(series).click();
        waitUntil(n1).click();
        waitUntil(machineType).click();
        waitUntil(n1Standard8).click();
        waitUntil(addGPUsCheckbox).click();
        waitUntil(gpyType).click();
        waitUntil(teslaP100).click();
        waitUntil(numberOfGPUs).click();
        waitUntil(oneGPU).click();
        waitUntil(localSSD).click();
        waitUntil(twoFor375).click();
        waitUntil(datacenterLocation).click();
        waitUntil(frankfurt).click();
        waitUntil(committedUsage).click();
        waitUntil(oneYear).click();
        return this;
    }

    public PricingCalculatorPage addToEstimate() {
        addToEstimateButton.click();
        return this;
    }

    public PricingCalculatorPage emailEstimate() {
        emailButton.click();
        return this;
    }

    public PricingCalculatorPage pastAndSendEmail() {
        emailInput.click();
        emailInput.sendKeys(Keys.chord(Keys.CONTROL, "v"));
        sendEmailButton.click();
        return this;
    }

    public String getProvisioningModelEstimate() {
        return getTextNode(getEstimateElement("Provisioning model"));
    }

    public String getInstanceTypeEstimate() {
        return getTextNode(getEstimateElement("Instance type"));
    }

    public String getRegionEstimate() {
        return getTextNode(getEstimateElement("Region"));
    }

    public String getLocalSSDEstimateEstimate() {
        return getTextNode(getEstimateElement("Local SSD"));
    }

    public String getCommitmentTermEstimate() {
        return getTextNode(getEstimateElement("Commitment term"));
    }

    public String getTotalEstimatedCost() {
        return totalEstimatedCost.getText();
    }

    private WebElement getEstimateElement(String element) {
        return driver.findElement(By.xpath("//*[@id='compute']//div[contains(text(), '" + element + "')]"));
    }

    private WebElement waitUntil(WebElement element) {
        return waitUntilClick(driver, element);
    }

}
