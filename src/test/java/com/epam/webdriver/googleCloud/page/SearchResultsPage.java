package com.epam.webdriver.googleCloud.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResultsPage {
    public WebDriver driver;
    @FindBy(xpath = "//div[@class = 'gs-title']/a[b = 'Google Cloud Pricing Calculator']")
    private WebElement resultLink;

    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public PricingCalculatorPage resultClick() {
        resultLink.click();
        return new PricingCalculatorPage(driver);
    }

}
