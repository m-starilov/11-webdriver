package com.epam.webdriver.googleCloud.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TempMailMailboxPage {

    public WebDriver driver;
    @FindBy(xpath = "//*[contains(text(),'Estimated Monthly Cost:')]")
    private WebElement estimatedMonthlyCost;

    public TempMailMailboxPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getEstimatedMonthlyCost() {
        for (int i = 0; i < 3; i++) {
            if (driver.switchTo().frame("ifmail")
                    .findElements(By.xpath("//*[contains(text(),'Estimated Monthly Cost:')]")).size() > 0) {
                break;
            } else {
                driver.navigate().refresh();
            }
        }
        return estimatedMonthlyCost.getText()
                .replaceAll("[a-zA-Z ]|:", "");
    }

}
