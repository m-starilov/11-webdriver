package com.epam.webdriver.googleCloud.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TempMailPage {

    public WebDriver driver;
    private static final String URL = "https://yopmail.com/en/email-generator";
    @FindBy(xpath = "//button[@id='cprnd']")
    private WebElement copyButton;
    @FindBy(xpath = "//span[contains(text(),'Check Inbox')]")
    private WebElement checkInboxButton;


    public TempMailPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public TempMailPage openPage() {
        driver.switchTo().newWindow(WindowType.TAB).get(URL);
        return this;
    }

    public TempMailPage copyToClipBoard() {
        copyButton.click();
        return this;
    }

    public TempMailMailboxPage checkInbox() {
        checkInboxButton.click();
        return new TempMailMailboxPage(driver);
    }

}
