package com.epam.webdriver.googleCloud.test;

import com.epam.webdriver.googleCloud.page.MainPage;
import com.epam.webdriver.googleCloud.page.PricingCalculatorPage;
import com.epam.webdriver.googleCloud.page.TempMailPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

import static com.epam.webdriver.util.PageHelper.switchTab;

public class EmailEstimateOfComputeEngineTest {
    private WebDriver driver;

    @BeforeMethod(alwaysRun = true)
    public void browserSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
    }

    @Test(description = "Task 4. Hardcore")
    public void shouldAddToEstimateComputeEngineCalculation() {
        PricingCalculatorPage calculatorPage = new MainPage(driver)
                .openPage()
                .searchPricingCalculator()
                .resultClick()
                .pickComputeEngineSection()
                .fillOutForm()
                .addToEstimate()
                .emailEstimate();
        TempMailPage tempMailPage = new TempMailPage(driver)
                .openPage()
                .copyToClipBoard();
        switchTab(driver);
        String siteCost = calculatorPage
                .pastAndSendEmail()
                .getTotalEstimatedCost();
        switchTab(driver);
        String mailCost = tempMailPage
                .checkInbox()
                .getEstimatedMonthlyCost();

        Assert.assertEquals(mailCost, siteCost);
    }

    @AfterMethod(alwaysRun = true)
    public void browserTearDown() {
        driver.quit();
        driver = null;
    }
}
