package com.epam.webdriver.googleCloud.test;

import com.epam.webdriver.googleCloud.page.MainPage;
import com.epam.webdriver.googleCloud.page.PricingCalculatorPage;
import com.epam.webdriver.googleCloud.page.SearchResultsPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class EstimateOfComputeEngineTest {
    private WebDriver driver;

    @BeforeMethod(alwaysRun = true)
    public void browserSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
    }

    @Test(description = "Task 3. Hurt Me Plenty")
    public void shouldAddToEstimateComputeEngineCalculation() {
        SearchResultsPage searchResultsPage = new MainPage(driver)
                .openPage()
                .searchPricingCalculator();
        PricingCalculatorPage calculatorPage = searchResultsPage
                .resultClick()
                .pickComputeEngineSection()
                .fillOutForm()
                .addToEstimate();

        Assert.assertEquals(calculatorPage.getProvisioningModelEstimate(), "Provisioning model: Regular");
        Assert.assertEquals(calculatorPage.getInstanceTypeEstimate(), "Instance type: n1-standard-8");
        Assert.assertEquals(calculatorPage.getRegionEstimate(), "Region: Frankfurt");
        Assert.assertEquals(calculatorPage.getLocalSSDEstimateEstimate(), "Local SSD: 2x375 GiB");
        Assert.assertEquals(calculatorPage.getCommitmentTermEstimate(), "Commitment term: 1 Year");
        Assert.assertEquals(calculatorPage.getTotalEstimatedCost(), "Total Estimated Cost: USD 4,024.56 per 1 month");
    }

    @AfterMethod(alwaysRun = true)
    public void browserTearDown() {
        driver.quit();
        driver = null;
    }
}
