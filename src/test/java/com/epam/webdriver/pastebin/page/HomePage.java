package com.epam.webdriver.pastebin.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class HomePage {
    private static final String URL = "https://pastebin.com/";
    public WebDriver driver;
    @FindBy(id = "postform-text")
    private WebElement textArea;
    @FindBy(id = "select2-postform-expiration-container")
    private WebElement expirationContainer;
    @FindBy(id = "select2-postform-format-container")
    private WebElement formatContainer;
    @FindBy(id = "postform-name")
    private WebElement pasteName;
    @FindBy(xpath = "//li[text()='10 Minutes']")
    private WebElement tenMinutes;
    @FindBy(xpath = "//li[text()='Bash']")
    private WebElement bash;
    @FindBy(xpath = "//button")
    private WebElement createNewPasteButton;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public HomePage openPage() {
        driver.get(URL);
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10));
        return this;
    }

    public HomePage inputText(String code) {
        textArea.sendKeys(code);
        return this;
    }

    public HomePage set10MinutesExpiration() {
        expirationContainer.click();
        tenMinutes.click();
        return this;
    }

    public HomePage setBashSyntaxHighlighting() {
        formatContainer.click();
        bash.click();
        return this;
    }

    public HomePage setName(String title) {
        pasteName.sendKeys(title);
        return this;
    }

    public PastePage createNewPaste() {
        createNewPasteButton.click();
        return new PastePage(driver);
    }

}
