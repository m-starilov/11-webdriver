package com.epam.webdriver.pastebin.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PastePage {
    public WebDriver driver;
    @FindBy(xpath = "//a[@href='/archive/bash']")
    private WebElement codeHighlight;
    @FindBy(xpath = "//textarea")
    private WebElement textArea;

    public PastePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean titleIs(String title) {
        return new WebDriverWait(driver, Duration.ofSeconds(5))
                .until(ExpectedConditions.titleIs(title));
    }

    public WebElement getSyntaxHighlight() {
        return codeHighlight;
    }

    public WebElement getRawText() {
        return textArea;
    }

}
