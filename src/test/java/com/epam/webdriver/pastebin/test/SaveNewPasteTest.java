package com.epam.webdriver.pastebin.test;

import com.epam.webdriver.pastebin.page.HomePage;
import com.epam.webdriver.pastebin.page.PastePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SaveNewPasteTest {
    private WebDriver driver;

    @BeforeMethod(alwaysRun = true)
    public void browserSetup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test(description = "Task 2. Bring It On")
    public void shouldCreateAndSaveNewPaste() {
        String codeText = "git config --global user.name  \"New Sheriff in Town\"\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force";
        String name = "how to gain dominance among developers";
        PastePage pastePage = new HomePage(driver)
                .openPage()
                .inputText(codeText)
                .setBashSyntaxHighlighting()
                .set10MinutesExpiration()
                .setName(name)
                .createNewPaste();

        Assert.assertTrue(pastePage.titleIs(name + " - Pastebin.com"));
        Assert.assertEquals(pastePage.getSyntaxHighlight().getText(), "Bash");
        Assert.assertEquals(pastePage.getRawText().getText(), codeText);
    }

    @AfterMethod(alwaysRun = true)
    public void browserTearDown() {
        driver.quit();
        driver = null;
    }
}
